bitBlog
=======

bitBlog is a simple blog using the TypeJax LaTeX typesetter. Blog entries can be saved in the "content" folder. list.txt is the list of documents that you'd like to have appear in the sidebar.

bitBlog is not affiliated with, endorsed or supported by JaxEdit.

Documentation for the supported commands/environments can be found here:
http://jaxedit.com/page/latex.html

You can write your blog posts using JaxEdit editor:
http://jaxedit.com/note/

This software is released under the GPLv3. Software (including JaxEdit) released in "vendor" folders are released under their own respective licenses.